import React, { Component } from "react";
import styled from "styled-components";

const StyledInputWrapper = styled.div`
  border-radius: 5px;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  width: 100%;
  margin-top: 10px;
  display: flex;
  height: 150px;
  justify-content: space-around;
`;
const StyledInput = styled.input`
  width: 80%;
  min-width: 200px;
  height: 100px;
  font-size: 18px;
  align-self: center;
  margin-left: 10px;
`;
const StyledButton = styled.button`
  width: 80px;
  height: 30px;
  border-radius: 5px;

  align-self: center;
  background-color: azure;
  font-size: 16px;
`;

class Input extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "Message ..." };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <StyledInputWrapper>
        <StyledInput
          type="text"
          value={this.state.value}
          onChange={this.handleChange}
          onFocus={() => {
            this.setState({ value: "" });
          }}
        ></StyledInput>
        <StyledButton
          onClick={() => {
            if (this.state.value !== "" && this.state.value !== "Message...") {
              this.props.addMessage(this.state.value);
            }
            this.setState({ value: "Message..." });
          }}
        >
          Send
        </StyledButton>
      </StyledInputWrapper>
    );
  }
}
export default Input;
