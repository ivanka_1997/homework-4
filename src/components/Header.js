import React, { Component } from "react";
import styled from "styled-components";

const StyledHeader = styled.header`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: space-between;
`;
const StyledParagraph = styled.p`
  font-size: 18px;
`;

class Header extends Component {
  render() {
    const { participantsNumber, messagesNumber, lastMessageTime } = this.props;

    return (
      <StyledHeader>
        <StyledParagraph>My chat</StyledParagraph>
        <StyledParagraph>{participantsNumber} participants</StyledParagraph>
        <StyledParagraph>{messagesNumber} messages</StyledParagraph>
        <StyledParagraph>Last message at: {lastMessageTime}</StyledParagraph>
      </StyledHeader>
    );
  }
}
export default Header;
