import React, { Component } from "react";
import styled from "styled-components";
import editIcon from "../images/edit.png";
import deleteIcon from "../images/delete.png";
import LikeIcon from "./LikeIcon";

const StyledMessageWrapper = styled.div`
  border: 2px solid red;
  border-radius: 5px;
  padding: 5px;
  width: fit-content;
  margin-top: 10px;
  display: flex;
  &.right {
    margin: 10px 0 auto auto;
    justify-content: space-between;
  }
`;

const StyledImg = styled.img`
  width: 40px;
  height: 40px;
  align-self: center;
  margin-left: 10px;
  &.icon {
    width: 20px;
    height: 20px;
    padding: 5px;
  }
`;

const StyledParagraph = styled.p`
  margin: 5px;
  font-size: 16px;
  &.messageTime {
    font-size: 12px;
    color: gray;
  }
`;

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  &.icon_wrapp {
    align-items: center;
  }
`;

class Message extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { userAvatar, messageText, messageTime, message_id } = this.props;

    return (
      <StyledMessageWrapper className={userAvatar === "" && "right"}>
        {userAvatar !== "" && (
          <StyledImg
            src={userAvatar}
            alt="avatar"
            onError={(e) => {
              e.target.src =
                "https://uybor.uz/borless/uybor/img/user-images/user_no_photo_100x100.png";
            }}
          />
        )}
        <StyledWrapper>
          <StyledParagraph>{messageText}</StyledParagraph>
          <StyledParagraph className="messageTime">
            {new Date(messageTime).toLocaleString()}
          </StyledParagraph>
        </StyledWrapper>
        {userAvatar === "" && (
          <StyledWrapper className="icon_wrapp">
            <StyledImg
              className="icon"
              src={deleteIcon}
              id={message_id}
              onClick={(e) => {
                this.props.deleteMessage(e.target.id);
              }}
            />
            <StyledImg
              className="icon"
              src={editIcon}
              id={message_id}
              onClick={(e) => {
                this.props.editMessage(e.target.id);
              }}
            />
          </StyledWrapper>
        )}
        {userAvatar !== "" && <LikeIcon fill="#fff" />}
      </StyledMessageWrapper>
    );
  }
}
export default Message;
