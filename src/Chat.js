import React, { Component } from "react";
import axios from "axios";
import { default as UUID } from "node-uuid";
import Header from "./components/Header";
import Message from "./components/Message";
import Input from "./components/Input";
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: auto;
  margin-top: 15%;
  border-color: red;
`;
const DATA_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      data: [],
    };
  }

  componentDidMount() {
    this.setState({ ...this.state, isFetching: true });
    setTimeout(() => {
      axios
        .get(DATA_URL)
        .then((response) => {
          this.setState({
            data: response.data,
            isFetching: false,
            lastTime: new Date(
              response.data[response.data.length - 1].createdAt
            ).toLocaleString(),
          });
        })
        .catch((e) => {
          console.log(e);
          this.setState({ ...this.state, isFetching: false });
        });
    }, 3000);
  }

  addNewMessage = (message) => {
    this.setState({
      data: [
        ...this.state.data,
        {
          id: UUID.v4(),
          userId: "5335ersd-1b8f-11e8-9629-c7eca82aa7bd",
          avatar: "",
          user: "You",
          text: message,
          createdAt: new Date().toISOString(),
          editedAt: new Date().toISOString(),
        },
      ],
      lastTime: new Date().toLocaleString(),
    });
  };

  deleteMessage = (id) => {
    const newData = [...this.state.data.filter((item) => item.id !== id)];
    this.setState({
      data: newData,
      lastTime: new Date(
        newData[newData.length - 1].createdAt
      ).toLocaleString(),
    });
  };

  editMessage = (id) => {};

  render() {
    if (this.state.isFetching)
      return (
        <ClipLoader
          color="red"
          loading={this.state.isFetching}
          css={override}
          size={150}
        />
      );

    return (
      <>
        <Header
          participantsNumber={
            new Set(this.state.data.map(({ userId }) => userId)).size
          }
          messagesNumber={this.state.data.length}
          lastMessageTime={this.state.lastTime}
        />
        {this.state.data.map(({ avatar, text, createdAt, id }) => (
          <Message
            userAvatar={avatar}
            messageText={text}
            messageTime={createdAt}
            message_id={id}
            key={id}
            deleteMessage={this.deleteMessage}
            editMessage={this.editMessage}
          />
        ))}
        <Input addMessage={this.addNewMessage} />
      </>
    );
  }
}

export default Chat;
